export const treeNewBeeStore = {
	namespaced: true,
	state: {
		dataSource: [],
		childrenItemObj: {}
	},
	getters: {
	
	},
	mutations: {
		modifyChildrenItemObj(state, item) {
			state.childrenItemObj = item
		},
		firstCreateChirldren(state, item) {
			state.dataSource.push(item)
		},
		createChildren(state, item, obj) {
			item.children.splice(index, 0, obj)
		}
	},
	actions: {
		getNewNum({ dispatch, commit, getters, rootGetters }, num) {   //同上注释，num为要变化的形参
			setTimeout(() => {
				commit('newNum',num)
			}, 500)
		}
	}
};
