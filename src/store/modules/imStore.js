import { createGroup } from "../../services/api/imApis";

export const imStore = {
  namespaced: true,
  state: {

  },
  getters: {

  },
  mutations: {

  },
  actions: {
    createGroupInStore(context, obj) {
      createGroup(obj).then(res => {
        console.log(res)
      })
    }
  }
};
