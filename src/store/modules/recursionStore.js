export const recursionStore = {
  namespaced: true,
  state: {
    list:[
      {
        show: true,
        showInput: false,
        name: "Root",
        level: 1,
        key: 1,
        parent: null,
        children:[
          {
            name: 'test',
            children: []
          }
        ],
      }
    ]
  },
  getters: {

  },
  mutations: {

  },
  actions: {

  }
}