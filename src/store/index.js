import Vue from 'vue';
import Vuex from 'vuex';
import { countStore } from './modules/countStore';
import { treeNewBeeStore } from "./modules/treeNewBeeStore";
import { imStore } from "./modules/imStore";
import { recursionStore } from "./modules/recursionStore";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    mouduleA: countStore,
    treeNewBeeStore,
    imStore,
    recursionStore
  }
});
export default store;
