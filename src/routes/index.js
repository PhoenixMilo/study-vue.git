import Router from 'vue-router'
import Vue from 'vue'

//没有指定webpackChunkName,每个组件打包成一个js文件
const Computed = () => import('../containers/Computed');
const BindClass = () => import('../containers/BindClass');
const VFor = () => import('../containers/VFor');
const Watch = () => import('../containers/Watch');
const VIf = () => import('../containers/VIf');
const Props = () => import('../containers/Props');
const RouterParams = () => import('../containers/RouterParams.vue');
const RouterQuery = () => import('../containers/RouterQuery');
const Slot = () => import('../containers/SlotCom');
const Components = () => import('../containers/Components');
const Vuex = () => import('../containers/Vuex');
const Transitions = () => import('../containers/Transitions');
const TencentIM = () => import('../containers/IM');
const VueTreeList = () => import('../containers/VueTreeList');
const VueSortableTree = () => import('../containers/VueSortableTree');
//指定了相同的webpackChunkName，会合并打包成y一个js文件
const TreeNewBee = () => import(/*webpackChunkName: 'hehe'*/'../containers/TreeNewBee');
const ReduceCom = () => import(/*webpackChunkName: 'hehe'*/'../containers/ReduceCom');

Vue.use(Router);

const routes = [
  {path: '/computed', component: Computed, name: 'Computed'},
  {path: '/bindclass', component: BindClass, name: 'BindClass'},
  {path: '/vfor', component: VFor, name: 'VFor'},
  {path: '/Watch', component: Watch, name: 'Watch'},
  {path: '/vif', component: VIf, name: 'VIf'},
  {path: '/props', component: Props, name: 'Props'},
  {path: '/routerparams/:id', component: RouterParams, name: 'RouterParams'},
  {path: '/routerquery', component: RouterQuery, name: 'RouterQuery'},
  {path: '/slot', component: Slot, name: 'Slot'},
  {path: '/components', component: Components, name: 'Components'},
  {path: '/vuex', component: Vuex, name: 'Vuex'},
  {path: '/transitions', component: Transitions, name: 'Transitions'},
  {path: '/tree', component: TreeNewBee, name: 'TreeNewBee'},
  {path: '/reduce', component: ReduceCom, name: 'ReduceCom'},
  {path: '/im', component: TencentIM, name: 'TencentIM'},
  {path: '/vuetreelist', component: VueTreeList, name: 'VueTreeList'},
  {path: '/vuesortabletree', component: VueSortableTree, name: 'VueSortableTree'},
];


export default new Router({
  routes
});
