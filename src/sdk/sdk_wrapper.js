import { format } from "../utils/utils";
require('./RongIMLib-2.5.0.js');
require('./protobuf-2.3.5.min.js');
require('./RongEmoji-2.2.7.js');
export var RongIMLib = window.RongIMLib;

var RongIMClient = RongIMLib.RongIMClient;

var config = {
  size: 24, // 大小, 默认 24, 建议18 - 58
  url: '//f2e.cn.ronghub.com/sdk/emoji-48.png', // Emoji 背景图片
  lang: 'zh', // Emoji 对应名称语言, 默认 zh
  // 扩展表情
  extension: {
    dataSource: {
      u1F914: {
        en: 'thinking face', // 英文名称
        zh: '思考', // 中文名称
        tag: '🤔', // 原生 Emoji
        position: '0 0' // 所在背景图位置坐标
      }
    },
    url: '//cdn.ronghub.com/thinking-face.png' // 新增 Emoji 背景图 url
  }
};
RongIMLib.RongIMEmoji.init(config);

var list = RongIMLib.RongIMEmoji.list;

export function init(params, addPromptInfo) {
  var appkey = params.appkey;
  var token = params.token;
  var navi = params.navi;
  var config = {};
  if (navi) {
    config.navi = navi
  }
  RongIMClient.init(appkey, null, config);
  RongIMClient.setConnectionStatusListener({
    onChanged: function (status) {
      switch (status) {
        case RongIMLib.ConnectionStatus['CONNECTED']:
        case 0:
          addPromptInfo('连接成功');
          break;

        case RongIMLib.ConnectionStatus['CONNECTING']:
        case 1:
          addPromptInfo('连接中');
          break;

        case RongIMLib.ConnectionStatus['DISCONNECTED']:
        case 2:
          addPromptInfo('当前用户主动断开链接');
          break;

        case RongIMLib.ConnectionStatus['NETWORK_UNAVAILABLE']:
        case 3:
          addPromptInfo('网络不可用');
          break;

        case RongIMLib.ConnectionStatus['CONNECTION_CLOSED']:
        case 4:
          addPromptInfo('未知原因，连接关闭');
          break;

        case RongIMLib.ConnectionStatus['KICKED_OFFLINE_BY_OTHER_CLIENT']:
        case 6:
          addPromptInfo('用户账户在其他设备登录，本机会被踢掉线');
          break;

        case RongIMLib.ConnectionStatus['DOMAIN_INCORRECT']:
        case 12:
          addPromptInfo('当前运行域名错误，请检查安全域名配置');
          break
      }
    }
  });

  RongIMClient.setOnReceiveMessageListener({
    // 接收到的消息
    onReceived: function (message) {
      let obj = {
        roomId: message.targetId,
        sendUserId: message.senderUserId,
        sentTime: format(message.sentTime),
        // sentTime: message.sentTime,
        content: RongIMLib.RongIMEmoji.symbolToEmoji(message.content.content)
      };
      addPromptInfo(obj)
    }
  });

  RongIMClient.connect(token, {
    onSuccess: function (userId) {
      addPromptInfo('链接成功，用户id：' + userId);
      localStorage.setItem('userId', userId)
    },
    onTokenIncorrect: function () {
      addPromptInfo('token无效')
    },
    onError: function (errorCode) {
      addPromptInfo(errorCode)
    }
  }, null);

  // 消息监听器
  RongIMClient.setOnReceiveMessageListener({
    // 接收到的消息
    onReceived: function (message) {
      // 判断消息类型
      switch (message.messageType) {
        case RongIMClient.MessageType.TextMessage:
          console.log(message.content.content);
          // message.content.content => 文字内容
          break;
        case RongIMClient.MessageType.VoiceMessage:
          // message.content.content => 格式为 AMR 的音频 base64
          break;
        case RongIMClient.MessageType.ImageMessage:
          // message.content.content => 图片缩略图 base64
          // message.content.imageUri => 原图 URL
          break;
        case RongIMClient.MessageType.LocationMessage:
          // message.content.latiude => 纬度
          // message.content.longitude => 经度
          // message.content.content => 位置图片 base64
          break;
        case RongIMClient.MessageType.RichContentMessage:
          // message.content.content => 文本消息内容
          // message.content.imageUri => 图片 base64
          // message.content.url => 原图 URL
          break;
        case RongIMClient.MessageType.InformationNotificationMessage:
          // do something
          break;
        case RongIMClient.MessageType.ContactNotificationMessage:
          // do something
          break;
        case RongIMClient.MessageType.ProfileNotificationMessage:
          // do something
          break;
        case RongIMClient.MessageType.CommandNotificationMessage:
          // do something
          break;
        case RongIMClient.MessageType.CommandMessage:
          // do something
          break;
        case RongIMClient.MessageType.UnknownMessage:
          // do something
          break;
        default:
        // do something
      }
    }
  });
}
