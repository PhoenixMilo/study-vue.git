// format time

export function format (sendTime) {
  let date = new Date(sendTime);
  let Y = date.getFullYear() + '-';
  let M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
  let D = date.getDate() + ' ';
  let h = date.getHours();
  if (h < 10) {
    h = '0' + h + ':'
  } else {
    h = h + ':'
  }
  let m = date.getMinutes();
  if (m < 10) {
    m = '0' + m + ':'
  } else {
    m = m + ':'
  }
  let s = date.getSeconds();
  if (s < 10) {
    s = '0' + s
  }
  return Y + M + D + h + m + s;
}
