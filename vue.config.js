
module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
  // outputDir: 在npm run build时 生成文件的目录 type:string, default:'dist'
  // outputDir: 'dist',
  // pages:{ type:Object,Default:undfind }
  devServer: {
    port: 8888, // 端口号
    host: 'localhost',
    https: false, // https:{type:Boolean}
    open: true, //配置自动启动浏览器
    proxy: 'http://192.168.1.47:8080'
    // proxy: {
    //   // 检测遇到的接口
    //   '/group/create': {
    //     // 代理地址
    //     target: 'http://api-cn.ronghub.com',
    //     ws: true,
    //     changeOrigin: true
    //   },
    //   '/foo': {
    //     target: '<other_url>'
    //   }
    // },
  }
};
